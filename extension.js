/* -*- mode: js2; js2-basic-offset: 4; indent-tabs-mode: nil -*- */
/*
    Copyright (C) 2013  Philippe Normand.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const Lang = imports.lang;
const Main = imports.ui.main;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;

const WorkspaceHijackerExtension = new Lang.Class({
    Name: 'WorkspaceHijackerExtension',

    _showWorkspaceSwitcher(display, window, binding) {
        let workspaceManager = display.get_workspace_manager();

        if (!Main.sessionMode.hasWorkspaces)
            return;

        if (workspaceManager.n_workspaces == 1)
            return;

        let [action,,,target] = binding.get_name().split('-');
        let newWs;
        let direction;
        let wm = Main.wm;

        if (action == 'move') {
            // "Moving" a window to another workspace doesn't make sense when
            // it cannot be unstuck, and is potentially confusing if a new
            // workspaces is added at the start/end
            if (window.is_always_on_all_workspaces() ||
                (Meta.prefs_get_workspaces_only_on_primary() &&
                 window.get_monitor() != Main.layoutManager.primaryIndex))
              return;
        }

        if (target == 'last') {
            direction = Meta.MotionDirection.DOWN;
            newWs = workspaceManager.get_workspace_by_index(workspaceManager.n_workspaces - 1);
        } else if (isNaN(target)) {
            // Prepend a new workspace dynamically
            if (workspaceManager.get_active_workspace_index() == 0 &&
                action == 'move' && target == 'up' && wm._isWorkspacePrepended == false) {
                wm.insertWorkspace(0);
                wm._isWorkspacePrepended = true;
            }

            direction = Meta.MotionDirection[target.toUpperCase()];
            newWs = workspaceManager.get_active_workspace().get_neighbor(direction);
        } else if (target > 0) {
            target--;
            newWs = workspaceManager.get_workspace_by_index(target);

            if (workspaceManager.get_active_workspace().index() > target)
                direction = Meta.MotionDirection.UP;
            else
                direction = Meta.MotionDirection.DOWN;
        }

        if (direction != Meta.MotionDirection.UP &&
            direction != Meta.MotionDirection.DOWN)
            return;

        if (action == 'switch')
            wm.actionMoveWorkspace(newWs);
        else
            wm.actionMoveWindow(window, newWs);
    },

    _configureKeybindings(cb) {
        let wm = Main.wm;
        wm.setCustomKeybindingHandler('switch-to-workspace-1',
                                      Shell.ActionMode.NORMAL |
                                      Shell.ActionMode.OVERVIEW,
                                      cb.bind(this));
        wm.setCustomKeybindingHandler('switch-to-workspace-2',
                                      Shell.ActionMode.NORMAL |
                                      Shell.ActionMode.OVERVIEW,
                                      cb.bind(this));
        wm.setCustomKeybindingHandler('switch-to-workspace-3',
                                      Shell.ActionMode.NORMAL |
                                      Shell.ActionMode.OVERVIEW,
                                      cb.bind(this));
        wm.setCustomKeybindingHandler('switch-to-workspace-4',
                                      Shell.ActionMode.NORMAL |
                                      Shell.ActionMode.OVERVIEW,
                                      cb.bind(this));
        wm.setCustomKeybindingHandler('switch-to-workspace-5',
                                      Shell.ActionMode.NORMAL |
                                      Shell.ActionMode.OVERVIEW,
                                      cb.bind(this));
        wm.setCustomKeybindingHandler('switch-to-workspace-6',
                                      Shell.ActionMode.NORMAL |
                                      Shell.ActionMode.OVERVIEW,
                                      cb.bind(this));
    },

    enable() {
        this._configureKeybindings(this._showWorkspaceSwitcher);
    },

    disable() {
        this._configureKeybindings(Main.wm._showWorkspaceSwitcher);
    }

});

function init() {
    return new WorkspaceHijackerExtension();
}
