This is a simple GNOME Shell extension to prevent the workspace
switcher from being shown when moving between workspaces.
